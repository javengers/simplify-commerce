from simplifysetup import *

authorization = simplify.Authorization.create({
        "amount" : "1100",
        "description" : "test authorization",
        "card" : {
           "expMonth" : "11",
           "expYear" : "19",
           "cvc" : "123",
           "number" : "5105105105105100"
        },
        "currency" : "USD"
})

print 'Authorization'
print authorization

payment = simplify.Payment.create({
        "amount" : "900",
        "description" : "shipment of two eggs in a glass bottle",
        "authorization" : authorization.id,
        "currency" : "USD"

})

print 'Payment'
print payment

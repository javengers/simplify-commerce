import simplify

simplify.public_key = ''
simplify.private_key = ''

if not simplify.public_key or not simplify.private_key:
    raise Exception('Provide public and private key')

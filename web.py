from flask import Flask, render_template, url_for, redirect, request
from simplifysetup import *

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/submit', methods=['POST'])
def submit():
    token = request.values.get('simplifyToken')
    amount = request.values.get('amount')
    print 'Token:' + token
    print 'Amount:' + amount

    payment = simplify.Payment.create({
        "amount": amount,
        "token": token,
        "description": "payment description",
        "currency": "USD"

    })
    print payment

    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)
